package com.epam.CalculatorTask;

import java.util.*;

class CalculatorApp {
	
	public int add(int a, int b) {
		return a + b;
	}
	
	public int difference(int a, int b) {
		return a - b;
    }
	
	public int mul(int a, int b) {
		return a * b;
	}
	
	public int div(int a,int b) {
		if(b == 0) {
			return -1;
	    } else {
	        return a / b;
	    }
    }
}


public class Calculator {
    public static void main(String[] args) {
    	int a,b;
    	char ch;
        Scanner sc = new Scanner(System.in);
        CalculatorApp calc = new CalculatorApp();
        System.out.println("Enter two numbers: ");
        a = sc.nextInt();
        b = sc.nextInt();
        System.out.println("Enter the operand: ");
        ch = sc.next().charAt(0);
        switch(ch) {
        	case '+':
        		System.out.println("Addition is " + calc.add(a, b));
        		break;
        	case '-':
        		System.out.println("Difference is " + calc.difference(a, b));
        		break;
        	case '*':
        		System.out.println("Product is " + calc.mul(a, b));
        		break;
        	case '/':
        		System.out.println("Division is " + calc.div(a, b));
        		break;
        	default:
        		System.out.println("Ener valid operator");
        		break;
        }
        sc.close();
    }
}
